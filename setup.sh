#!/bin/bash

# Author: Talin Paul
# Copyright (c) fireworks.inc

echo 'Install Symlinks...'

# Creating Sym links
ln -s -f ~/.dotfiles/.bash_logout ~/.bash_logout
ln -s -f ~/.dotfiles/.bash_aliases ~/.bash_aliases
ln -s -f ~/.dotfiles/.bashrc ~/.bashrc
ln -s -f ~/.dotfiles/.profile ~/.profile
ln -s -f ~/.dotfiles/.curlrc ~/.curlrc
ln -s -f ~/.dotfiles/.wgetrc ~/.wgetrc
ln -s -f ~/.dotfiles/.inputrc ~/.inputrc
ln -s -f ~/.dotfiles/.gitconfig ~/.gitconfig
ln -s -f ~/.dotfiles/.gitignore ~/.gitignore
ln -s -f ~/.dotfiles/.gitmessage ~/.gitmessage
ln -s -f ~/.dotfiles/.npmrc ~/.npmrc
ln -s -f ~/.dotfiles/.vimrc ~/.vimrc
ln -s -f ~/.dotfiles/.vimrc.bundles ~/.vimrc.bundles
ln -s -f ~/.dotfiles/.ctags ~/.ctags

echo 'Install Git hooks...'
# Install Git Hooks
cd ~
git clone git@bitbucket.org:talinpaul/.hooks.git
ln -s -f ~/.hooks/.dotfiles/* ~/.dotfiles/.git/hooks
cd .dotfiles

echo 'Install Templates...'
# Install Templates
ln -s -f ~/.dotfiles/templates/* ~/Templates

echo 'Install Fonts...'
# Install Fonts
sudo ln -s -f ~/.dotfiles/fonts/* /usr/local/share/fonts

clear

echo 'Install Stubby (DNS Over Https)...'
# DNS over HTTPS
sudo apt-get install stubby -y
sudo systemctl start stubby
sudo systemctl enable stubby

clear

echo 'Install Nodejs...'
# Install Nodejs
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt update && sudo apt install nodejs -y

clear

echo 'Install MongoDB...'
# Install Mongod
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
sudo apt update && sudo apt install mongodb-org -y

clear

# Install Redis
echo 'Install Redis...'
sudo apt install redis-server -y
sudo systemctl enable redis-server.service

clear

# Install Java
echo 'Install Java...'
sudo apt install default-jdk

clear

echo 'Install other Essential Softwares...'
sudo apt install ubuntu-restricted-extras -y
sudo apt install vim -y
sudo apt install exuberant-ctags -y

clear

# Create a Working Directory
mkdir working && cd working

# Install Pureline
git clone https://github.com/powerline/fonts.git # Download Pureline fonts
./fonts/install.sh # Install Pureline fonts
rm -rf fonts # Clean up... We don't need this anymore.

# Clean up Working Directory
cd .. && rm -rf working

clear

# Update OS
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y

# Clean up
sudo apt autoremove -y
