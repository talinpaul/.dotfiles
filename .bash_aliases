# Allow aliases to be with sudo
alias sudo="sudo "

# Easier navigation: .., ..., ...., ....., ~ and -
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ~="cd ~" # `cd` is probably faster to type though

# alias mkcd="f() { echo $1; mkdir -p -- \"$1\" && cd -P -- \"$1\"; }; f"
alias mkcd='_(){ mkdir "$1"; cd "$1"; }; _'

# IP addresses
alias ip="dig +short myip.opendns.com @resolver1.opendns.com"

# Clipboard
alias clipboard='xclip -sel clip'

# NPM
alias ni="npm install"
alias nid="npm install --save-dev"
alias nig="npm install -g"
alias ns="npm start"
alias nw="npm run watch"
alias nr="npm run-script"
alias nt="npm test"
# alias nit="npm install && npm test"
# alias nis="npm install && npm start"
# alias nits="npm install && npm test && npm start"
# alias nl="npm link"